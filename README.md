This repo for handover

Tuyet Le Anh (IT - EA&Strategy) <tuyetla@vpbank.com.vn>; 
Quang Nguyen Tuan (IT - EA&Strategy) <quangnt3@vpbank.com.vn>; 
Amit Midha(IT - EA&amp;Strategy) <amitmidha@vpbank.com.vn>

| Default aligned | Left aligned | Center aligned  | Right aligned  |
|-----------------|:-------------|:---------------:|---------------:|
| First body part | Second cell  | Third cell      | fourth cell    |
| Second line     | foo          | **strong**      | baz            |
| Third line      | quux         | baz             | bar            |
|-----------------+--------------+-----------------+----------------|
| Second body     |              |                 |                |
| 2nd line        |              |                 |                |
|-----------------+--------------+-----------------+----------------|
| Third body      |              |                 | Foo            |
{: .custom-class #custom-id}


<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/enMumwvLAug" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

